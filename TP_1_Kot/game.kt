class Game {

   
    companion object{

    var result = (0..100).random()
    var countRound : Int = 0
    var userEntry : Int  = 0 

    var playAgain : Boolean = true 

    fun initialize(){
        result = (0..100).random() 
        userEntry = 0     
    }


    fun getEntryUser() : Int{

          return readLine()!!.toInt()

    }


    fun verify(value : Int) : Boolean{

            if(result == value){
                
                return true

            }else{

                if(value > result){
                    println("Le nombre à trouver est plus pétit")

                }else{
                     println("Le nombre à trouver est plus grand")

                }

                return false
            }

    }

    fun play(){
            
            while(playAgain){

                 println("Devinez le nombre secret")
              
                    do{
                
                    userEntry = getEntryUser()
                    println(userEntry)
                    countRound = countRound + 1
                    }while(!verify(userEntry))

              println("Bravo vous avez trouvez le nombre en ${countRound} tour(s)")
             
              initialize()
              
        }

        }

    

    }

}

fun main(){

    Game.play()

}
