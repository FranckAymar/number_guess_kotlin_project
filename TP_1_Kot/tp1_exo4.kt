import *

fun main(){


    val listBook = mutableListOf<Book>()

    listBook.add(Book("Title 1", "Franck 1"))
    listBook.add(Book("Title 2", "Franck 2"))
    listBook.add(Book("Title 3", "Franck 3"))

    for(book in listBook){

        println("Title : ${book.title} Author : ${book.author}")
    }
    
}
