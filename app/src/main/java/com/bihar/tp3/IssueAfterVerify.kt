package com.bihar.tp3

enum class IssueAfterVerify {

    CORRECT, WRONG_HIGHEST, WRONG_LOWEST

}