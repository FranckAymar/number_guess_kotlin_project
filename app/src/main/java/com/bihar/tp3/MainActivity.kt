package com.bihar.tp3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.bihar.tp3.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {


    var result : Int? = 0
    var countRound : Int = 0
    var userEntry : Int  = 0

    var listScore = mutableListOf<Int>()

   /* private lateinit var editNombre : EditText
    private lateinit var btnJouer : Button
    private lateinit var btnReset : Button
    private lateinit var txtResult : TextView
    private lateinit var btnScore : Button*/

    private lateinit var binding : ActivityMainBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_main)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        result = (0..100).random()
        Log.e("Result", result!!.toString())



        /*editNombre = findViewById(R.id.edit_nombre)
        btnJouer = findViewById(R.id.btn_jouer)
        btnReset = findViewById(R.id.btn_reset)
        txtResult = findViewById(R.id.txt_result)
        btnScore = findViewById(R.id.btn_scores)*/

        //



        binding.btnJouer.setOnClickListener {

            if(binding.editNombre.text.toString() != ""){



            userEntry = binding.editNombre.text.toString().toInt()
            countRound += 1


            showResult(result!!, userEntry)


        }else{
                Toast.makeText(this@MainActivity, "Veuillez entrer une valeur", Toast.LENGTH_SHORT).show()

            }


        }


        binding.btnReset.setOnClickListener {

            reset()
        }

        binding.btnScores.setOnClickListener {

            goToScoreActivity()
        }




    }



    fun reset(){

        //binding.txtResult.setText("")
        result = (0..100).random()
        Log.e("result", result.toString())
        countRound = 0
        Toast.makeText(this@MainActivity, "Rénitialiser avec succès", Toast.LENGTH_SHORT).show()
        binding.editNombre.setText("")

    }




    fun goToScoreActivity(){


        val intent = Intent(this, ScoreActivity::class.java)
        for (string in  listScore.toIntArray().sortedArrayDescending() ){
            Log.i("list",  string.toString())
        }
        intent.putExtra("scores", listScore.toIntArray().sortedArray())
        startActivity(intent)
    }



    fun verify(result : Int, userEntry : Int) : IssueAfterVerify {



        if(result == userEntry){

            listScore.add(countRound)
            reset()

            return IssueAfterVerify.CORRECT


        }else{

            if(userEntry > result!!){

                return IssueAfterVerify.WRONG_LOWEST


            }else{

                return IssueAfterVerify.WRONG_HIGHEST


            }


        }



    }


    fun showResult(result : Int, userEntry : Int){

        when (verify(result, userEntry)){

            IssueAfterVerify.WRONG_LOWEST -> {

                binding.txtResult.setText("Le nombre à trouver est plus pétit")

            }

            IssueAfterVerify.WRONG_HIGHEST -> {

                binding.txtResult.setText("Le nombre à trouver est plus grand")

            }

            IssueAfterVerify.CORRECT -> {

                binding.txtResult.setText("Vous avez gagné en ${countRound} tours")

            }


        }

    }



}
