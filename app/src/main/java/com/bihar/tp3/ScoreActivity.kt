package com.bihar.tp3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import com.bihar.tp3.databinding.ActivityScoreBinding

class ScoreActivity : AppCompatActivity() {

   /* private lateinit var txtScore1 : TextView
    private lateinit var txtScore2 : TextView
    private lateinit var txtScore3 : TextView*/

    private lateinit var binding : ActivityScoreBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_score)

        /*txtScore1 = findViewById(R.id.score1)
        txtScore2 = findViewById(R.id.score2)
        txtScore3 = findViewById(R.id.score3)*/

        binding = DataBindingUtil.setContentView(this, R.layout.activity_score)

        val scoresSet = intent.getIntArrayExtra("scores")?.toSortedSet()

        val setIterator = scoresSet?.iterator()



        when (scoresSet?.size){

            0 -> {
                binding.score1.setText("Aucun meilleur score")
                binding.score2.setText("Aucun meilleur score")
                binding.score3.setText("Aucun meilleur score")

            }

            1 -> {
                binding.score1.setText(setIterator?.next().toString() + "Tours")
                binding.score2.setText("Pas encore disponible")
                binding.score3.setText("Pas encore disponible")


            }
            2 -> {
                binding.score1.setText(setIterator?.next().toString() + "Tours")
                binding.score2.setText(setIterator?.next().toString() + "Tours")
                binding.score3.setText("Pas encore disponible")


            }
            else ->{
                binding.score1.setText(setIterator?.next().toString() + "Tours")
                binding.score2.setText(setIterator?.next().toString() + "Tours")
                binding.score3.setText(setIterator?.next().toString() + "Tours")

            }

        }



    }
}
